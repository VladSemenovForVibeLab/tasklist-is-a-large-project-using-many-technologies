package com.semenov.tasklist.web.mappers;

import com.semenov.tasklist.domain.task.Task;
import com.semenov.tasklist.domain.task.TaskImage;
import com.semenov.tasklist.web.dto.task.TaskDto;
import com.semenov.tasklist.web.dto.task.TaskImageDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TaskImageMapper extends Mappable<TaskImage, TaskImageDto>{
}

package com.semenov.tasklist.web.mappers;

import com.semenov.tasklist.domain.task.Task;
import com.semenov.tasklist.web.dto.task.TaskDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TaskMapper extends Mappable<Task,TaskDto>{
}

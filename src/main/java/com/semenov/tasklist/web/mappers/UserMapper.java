package com.semenov.tasklist.web.mappers;

import com.semenov.tasklist.domain.user.User;
import com.semenov.tasklist.web.dto.user.UserDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper extends Mappable<User,UserDto>{
}

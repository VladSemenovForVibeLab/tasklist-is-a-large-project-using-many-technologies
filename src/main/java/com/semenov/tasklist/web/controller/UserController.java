package com.semenov.tasklist.web.controller;

import com.semenov.tasklist.domain.task.Task;
import com.semenov.tasklist.domain.user.User;
import com.semenov.tasklist.service.TaskService;
import com.semenov.tasklist.service.UserService;
import com.semenov.tasklist.web.dto.task.TaskDto;
import com.semenov.tasklist.web.dto.user.UserDto;
import com.semenov.tasklist.web.dto.validation.OnCreate;
import com.semenov.tasklist.web.dto.validation.OnUpdate;
import com.semenov.tasklist.web.mappers.TaskMapper;
import com.semenov.tasklist.web.mappers.UserMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
@Validated
@Tag(name = "User Controller",description = "User API")
public class UserController {
    private final UserService userService;
    private final TaskService taskService;
    private final UserMapper userMapper;
    private final TaskMapper taskMapper;
    @PutMapping
    @PreAuthorize("@customSecurityExpression.canAccessUser(#dto.id)")
    @Operation(summary = "Update User")
    public UserDto update(@Validated(OnUpdate.class) @RequestBody UserDto dto){
        User user = userMapper.toEntity(dto);
        User updatedUser = userService.update(user);
        return userMapper.toDto(updatedUser);
    }
    @GetMapping("/{id}")
    @PreAuthorize("@customSecurityExpression.canAccessUser(#id)")
    @Operation(summary = "Get UserDto by Id")
    public UserDto getById(@PathVariable Long id){
        User user = userService.getById(id);
        return userMapper.toDto(user);
    }
    @DeleteMapping("/{id}")
    @PreAuthorize("@customSecurityExpression.canAccessUser(#dto.id)")
    @Operation(summary = "Delete User by Id")
    public void deleteById(@PathVariable Long id){
        userService.delete(id);
    }

    @GetMapping("/{id}/tasks")
    @PreAuthorize("@customSecurityExpression.canAccessUser(#id)")
    @Operation(summary = "Gel all user tasks")
    public List<TaskDto> getTasksByUser(@PathVariable Long id){
        List<Task> tasks = taskService.getAllByUserId(id);
        return taskMapper.toDto(tasks);
    }

    @PostMapping("/{id}/tasks")
    @PreAuthorize("@customSecurityExpression.canAccessUser(#id)")
    @Operation(summary = "Add task to User")
    public TaskDto createTask(@PathVariable Long id,
                              @Validated(OnCreate.class)
                              @RequestBody TaskDto dto){
        Task task = taskMapper.toEntity(dto);
        Task createTask = taskService.create(task,id);
        return taskMapper.toDto(createTask);

    }
}

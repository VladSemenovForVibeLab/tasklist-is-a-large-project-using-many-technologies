package com.semenov.tasklist.web.controller;

import com.semenov.tasklist.domain.task.Task;
import com.semenov.tasklist.domain.task.TaskImage;
import com.semenov.tasklist.service.TaskService;
import com.semenov.tasklist.web.dto.task.TaskDto;
import com.semenov.tasklist.web.dto.task.TaskImageDto;
import com.semenov.tasklist.web.dto.validation.OnUpdate;
import com.semenov.tasklist.web.mappers.TaskImageMapper;
import com.semenov.tasklist.web.mappers.TaskMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.hibernate.query.sql.internal.ParameterRecognizerImpl;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/tasks")
@RequiredArgsConstructor
@Validated
@Tag(name = "Task Controller",description = "Task API")
public class TaskController {
    private final TaskImageMapper taskImageMapper;
    private final TaskService taskService;
    private final TaskMapper taskMapper;
    @PutMapping
    @PreAuthorize("canAccessTask(#dto.id)")
    @Operation(summary = "Update Task")
    public TaskDto update(@Validated(OnUpdate.class)@RequestBody TaskDto dto){
        Task task = taskMapper.toEntity(dto);
        Task updatedTask = taskService.update(task);
        return taskMapper.toDto(updatedTask);
    }
    @GetMapping("/{id}")
    @PreAuthorize("canAccessTask(#id)")
    @Operation(summary = "Get TaskDto by taskId ")
    public TaskDto getById(@PathVariable Long id){
        Task task = taskService.getById(id);
        return taskMapper.toDto(task);
    }
    @DeleteMapping("/{id}")
    @PreAuthorize("canAccessTask(#id)")
    @Operation(summary = "Delete task by taskId")
    public void deleteById(@PathVariable Long id){
        taskService.delete(id);
    }

    @PostMapping("/{id}/image")
    @Operation(summary = "Upload image to task")
    @PreAuthorize("canAccessTask(#id)")
    public void uploadImage(@PathVariable Long id,
                            @Validated @ModelAttribute TaskImageDto imageDto){
        TaskImage image = taskImageMapper.toEntity(imageDto);
        taskService.uploadImage(id,image);
    }
}

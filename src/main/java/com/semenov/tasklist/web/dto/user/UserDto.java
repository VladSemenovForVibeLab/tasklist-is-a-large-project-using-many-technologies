package com.semenov.tasklist.web.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.semenov.tasklist.domain.task.Task;
import com.semenov.tasklist.domain.user.Role;
import com.semenov.tasklist.web.dto.validation.OnCreate;
import com.semenov.tasklist.web.dto.validation.OnUpdate;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.util.List;
import java.util.Set;

@Data
@Schema(description = "UserDto")
public class UserDto {
    @NotNull(message = "Id must be not null",groups = OnUpdate.class)
    @Schema(description = "User Id",example = "1")
    private Long id;
    @Schema(description = "User name",example = "Vlad Semenov")
    @NotNull(message = "Name must be not null",groups = {OnCreate.class, OnUpdate.class})
    @Length(max = 255,message = "Name length must be smaller then 255 symbols",groups = {OnUpdate.class, OnUpdate.class})
    private String name;
    @Schema(description = "User email",example = "ooovladislavchik@gmail.com")
    @NotNull(message = "Username must be not null",groups = {OnCreate.class, OnUpdate.class})
    @Length(max = 255,message = "Username length must be smaller then 255 symbols",groups = {OnUpdate.class, OnUpdate.class})
    private String username;
    @Schema(description = "User crypted password",example = "$2a$12$uxzId883Fo7OJkO2kLhMAOgGbDWtz7OxyZeOba4FsX6RrPisacKvq")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotNull(message = "Password must be not null",groups = {OnCreate.class, OnUpdate.class})
    private String password;
    @Schema(description = "User password confirmation",example = "$2a$12$uxzId883Fo7OJkO2kLhMAOgGbDWtz7OxyZeOba4FsX6RrPisacKvq")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotNull(message = "Password confirmation must be not null",groups = {OnCreate.class})
    private String passwordConfirmation;
}

package com.semenov.tasklist.web.dto.auth;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
@Schema(description = "Request for login")
public class JWTRequest {
    @Schema(description = "email",example = "vlad@gmail.com")
    @NotNull(message = "Username must be not null")
    private String username;
    @Schema(description = "password for request",example = "123")
    @NotNull(message = "Password must be not null")
    private String password;
}

package com.semenov.tasklist.service;

import com.semenov.tasklist.domain.task.TaskImage;

public interface ImageService {
    String upload(TaskImage image);
}

package com.semenov.tasklist.service;

import com.semenov.tasklist.web.dto.auth.JWTRequest;
import com.semenov.tasklist.web.dto.auth.JWTResponse;

public interface AuthService {
    JWTResponse login(JWTRequest loginRequest);

    JWTResponse refresh(String refreshToken);
}

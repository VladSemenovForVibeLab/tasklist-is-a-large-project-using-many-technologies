package com.semenov.tasklist.domain.user;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN
}

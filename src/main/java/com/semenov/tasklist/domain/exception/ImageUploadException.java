package com.semenov.tasklist.domain.exception;

public class ImageUploadException extends RuntimeException{
    public ImageUploadException(String message){
        super(message);
    }
}

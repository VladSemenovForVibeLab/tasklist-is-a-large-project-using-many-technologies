insert into users(name,username,password)
values ('Vlad Semenov','vlad@gmail.com','$2a$12$uxzId883Fo7OJkO2kLhMAOgGbDWtz7OxyZeOba4FsX6RrPisacKvq'),
       ('Toly Pechkin','toli@gmail.com','$2a$12$uxzId883Fo7OJkO2kLhMAOgGbDWtz7OxyZeOba4FsX6RrPisacKvq');
insert into tasks (title,description,status,expiration_date)
values ('Bye mobile','BECAUSE','TODO','2023-01-29 12:00:00'),
       ('do homework','math','IN_PROGRESS','2023-01-29 12:00:00');
insert into users_tasks (task_id,user_id)
values (1,1),
       (2,2);
insert into users_roles(user_id,role)
values (1,'ROLE_ADMIN'),
       (1,'ROLE_USER'),
       (2,'ROLE_USER')